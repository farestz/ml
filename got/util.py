from pandas import DataFrame
from matplotlib import pyplot as plt


def validate(dataset):
    dataset.age = dataset.age.median()
    dataset.isAliveMother[dataset.isAliveMother.isnull()] = 0
    dataset.isAliveFather[dataset.isAliveFather.isnull()] = 0
    dataset.isAliveHeir[dataset.isAliveHeir.isnull()] = 0
    dataset.isAliveSpouse[dataset.isAliveSpouse.isnull()] = 0
    return dataset


def score(train, target):
    from sklearn import cross_validation, svm
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.linear_model import LogisticRegression

    knc_model = KNeighborsClassifier(n_neighbors=18)
    rfc_model = RandomForestClassifier(n_estimators=250)
    lr_model = LogisticRegression(penalty='l1', tol=.01)
    svc_model = svm.SVC()

    values = {}
    scores = cross_validation.cross_val_score(knc_model, train, target, cv=5)
    values['KNC'] = scores.mean()
    scores = cross_validation.cross_val_score(rfc_model, train, target, cv=5)
    values['RFC'] = scores.mean()
    scores = cross_validation.cross_val_score(lr_model, train, target, cv=5)
    values['LR'] = scores.mean()
    scores = cross_validation.cross_val_score(svc_model, train, target, cv=5)
    values['SVC'] = scores.mean()
    DataFrame.from_dict(values, orient='index').plot(kind='bar', legend=False)
    plt.show()
