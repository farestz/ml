from util import validate
from pandas import read_csv, DataFrame, options

from sklearn.preprocessing import normalize
from sklearn.ensemble import RandomForestClassifier

options.mode.chained_assignment = None
data = read_csv('character-predictions.csv')
drop_columns = ['culture',
                'S.No',
                'name',
                'title',
                'dateOfBirth',
                'DateoFdeath',
                'mother',
                'father',
                'heir',
                'house',
                'spouse',
                'isAlive']

target = data.isAlive[:25]
test = data.drop(drop_columns, axis=1)
train = test[:25]

train = normalize(validate(train))
test = validate(test)

model = RandomForestClassifier(n_estimators=250)
model.fit(train, target)
predict = model.predict(test)

result, isAlive = DataFrame(data.name), DataFrame(data.isAlive)
result.insert(1, 'isAlive', isAlive)
result.insert(2, 'isAlive_predict', predict)
result.to_csv('predict.csv', index=False, sep=';')
