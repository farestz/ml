from matplotlib import pyplot as plt
from pandas import read_csv, options, DataFrame

from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier

data = read_csv('train.csv')
options.mode.chained_assignment = None
drop_columns = ['PassengerId', 'Name', 'Ticket', 'Cabin']

# гипотеза: чем выше класс, тем выше вероятность спасения
data.pivot_table('PassengerId', 'Pclass', 'Survived', 'count').plot(kind='bar', stacked=True)

# гипотеза: больше выживших мужчин или женщин?
data.pivot_table('PassengerId', 'Sex', 'Survived', 'count').plot(kind='bar')

# гипотеза: вероятность выжить больше у "пассажиров-одиночек"
figure, axes = plt.subplots(ncols=2)
data.pivot_table('PassengerId', 'SibSp', 'Survived', 'count').plot(ax=axes[0], title='Relatives')
data.pivot_table('PassengerId', 'Parch', 'Survived', 'count').plot(ax=axes[1], title='Spouse, children')

data = data.drop(drop_columns, axis=1)
data.Age[data.Age.isnull()] = data.Age.median()
data.Embarked[data.Embarked.isnull()] = data.Embarked.mode()[0]
data = data.apply(LabelEncoder().fit_transform)

test = read_csv('test.csv')
test.Age[test.Age.isnull()] = test.Age.median()
test.Embarked[test.Embarked.isnull()] = test.Embarked.mode()[0]

result = DataFrame(test.Name)
result.insert(1, 'Class', test.Pclass)
result.insert(2, 'Sex', test.Sex)
result.insert(3, 'Age', test.Age)

test = test.drop(drop_columns, axis=1)
test = test.apply(LabelEncoder().fit_transform)

target = data.Survived
train = data.drop('Survived', axis=1)
rfc_model = RandomForestClassifier(n_estimators=250)
rfc_model.fit(train, target)
predict = rfc_model.predict(test)

result.insert(4, 'Survived', predict)
result.to_csv('predict.csv', index=False, sep=';')
# plt.show()
