from util import validate
from numpy import set_printoptions
from pandas import read_csv, options

from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, RFE, chi2

set_printoptions(precision=3)
options.mode.chained_assignment = None
data = read_csv('../got/character-predictions.csv')
drop_columns = ['culture',
                'S.No',
                'name',
                'title',
                'dateOfBirth',
                'DateoFdeath',
                'mother',
                'father',
                'heir',
                'house',
                'spouse',
                'isAlive']

target = data.isAlive[:100]
train = data.drop(drop_columns, axis=1)[:100]
train = validate(train)

# одномерный отбор
f_selection = SelectKBest(score_func=chi2, k=4)
fit = f_selection.fit(train, target)
features = fit.transform(train)
print(fit.scores_)

# рекурсивный
rfe = RFE(LogisticRegression(), n_features_to_select=5)
fit = rfe.fit(train, target)
print('Num features: {}'.format(fit.n_features_))
print('Selected features: {}'.format(fit.support_))
print('Feature ranking: {}'.format(fit.ranking_))
