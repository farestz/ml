from pandas import read_csv
from numpy import set_printoptions

from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, RFE, chi2

set_printoptions(precision=3)
url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data'
names = ['Pregnant Times', 'Plasma', 'Pressure', 'Skin', 'Insulin', 'Body Index', 'Pedi', 'Age', 'Class']
data = read_csv(url, names=names)

target = data.Class
train = data.drop('Class', axis=1)

# одномерный отбор признакоы
f_selection = SelectKBest(score_func=chi2, k=4)
fit = f_selection.fit(train, target)
features = fit.transform(train)
print(features)

# рекурсивное исключение признаков
rfe = RFE(LogisticRegression(), n_features_to_select=3)
fit = rfe.fit(train, target)
print('Num features: {}'.format(fit.n_features_))
print('Selected features: {}'.format(fit.support_))
print('Feature ranking: {}'.format(fit.ranking_))
