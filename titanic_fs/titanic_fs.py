import util
from itertools import cycle

from numpy import unique
from matplotlib import pyplot as plt
from pandas import DataFrame, options, read_csv

from sklearn.cluster import MeanShift
from sklearn.feature_selection import RFE, RFECV
from sklearn.ensemble import RandomForestClassifier

# отключение предупреждений библиотеки Pandas
options.mode.chained_assignment = None

# работа с входными данными (обучающая выборка)
data = read_csv('train.csv')
data = util.validate(data)

# сохранение данных тестовой выборки для вывода результата
test = read_csv('test.csv')
result = DataFrame(test.Name)
result.insert(1, 'Class', test.Pclass)
result.insert(2, 'Sex', test.Sex)
result.insert(3, 'Age', test.Age)
result.insert(4, 'Fare', test.Fare)

# работа с тестовыми данными (тестовая выборка)
test = util.validate(test)

# x и y
target = data.Survived
train = data.drop('Survived', axis=1)

# адекватность модели с изначальными признаками
scores = util.score(train, target)
util.plot_from_dict(scores, kind='bar', title='Without Feature Selection', ylim=(.0, 1.))
print('Max score: {}'.format(max(scores.values())))

# обучение и результат
rfc_model = RandomForestClassifier()
rfc_model.fit(train, target)
result.insert(5, 'Survived', rfc_model.predict(test))

# определение оптимального числа признаков
rfecv = RFECV(RandomForestClassifier(), cv=10, scoring='accuracy')
rfecv.fit(train, target)
n_features = rfecv.n_features_
print("Optimal number of features : {}".format(n_features))

# окно графика
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)

# feature selection - используется рекурсивное исключение признаков
rfe = RFE(RandomForestClassifier(), n_features_to_select=n_features)
fit = rfe.fit(train, target)
print('Selected features: {}'.format(fit.support_))
print('Feature ranking: {}'.format(fit.ranking_))

# отсечение признаков
drop_features = ['Parch', 'Embarked']
train = train.drop(drop_features, axis=1)
test = test.drop(drop_features, axis=1)

# адекватность модели с отобранными признаками
scores = util.score(train, target)
util.plot_from_dict(scores, kind='bar', title='With Feature Selection', ylim=(.0, 1.))
print('Max score with FS: {}'.format(max(scores.values())))

# обучение и результат
rfc_model = RandomForestClassifier()
rfc_model.fit(train, target)
result.insert(6, 'Survived_FS', rfc_model.predict(test))
result.insert(7, 'Equal', result.Survived == result.Survived_FS)
result.to_csv('predict.csv', sep=';', index=False)

# кластеризация
train = train.drop(['Pclass', 'SibSp', 'Sex'], axis=1)
train = train.as_matrix()

ms = MeanShift()
ms.fit(train)
labels = ms.labels_
cluster_centers = ms.cluster_centers_
n_clusters_ = len(unique(labels))

# окно графика
plt.figure()
colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(n_clusters_), colors):
    my_members = labels == k
    cluster_center = cluster_centers[k]
    plt.plot(train[my_members, 0], train[my_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=14)
plt.title('Estimated number of clusters:{}'.format(n_clusters_))
plt.show()
