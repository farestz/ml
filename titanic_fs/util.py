from pandas import DataFrame as PlotData


def validate(dataset):
    if dataset.empty:
        return None
    from sklearn.preprocessing import LabelEncoder
    drop_columns = ['PassengerId', 'Name', 'Ticket', 'Cabin']
    dataset = dataset.drop(drop_columns, axis=1)
    dataset.Age[dataset.Age.isnull()] = dataset.Age.median()
    dataset.Embarked[dataset.Embarked.isnull()] = dataset.Embarked.mode()[0]
    dataset = dataset.apply(LabelEncoder().fit_transform)
    return dataset


def score(x, y):
    from sklearn import cross_validation, svm
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.linear_model import LogisticRegression, SGDClassifier

    svc_model = svm.SVC()
    sgd_model = SGDClassifier()
    knc_model = KNeighborsClassifier()
    rfc_model = RandomForestClassifier()
    lr_model = LogisticRegression()

    cv = 10
    scores = {}
    score_value = cross_validation.cross_val_score(svc_model, x, y, cv=cv)
    scores['SVC'] = score_value.mean()
    score_value = cross_validation.cross_val_score(knc_model, x, y, cv=cv)
    scores['KNC'] = score_value.mean()
    score_value = cross_validation.cross_val_score(rfc_model, x, y, cv=cv)
    scores['RFC'] = score_value.mean()
    score_value = cross_validation.cross_val_score(lr_model, x, y, cv=cv)
    scores['LR'] = score_value.mean()
    score_value = cross_validation.cross_val_score(sgd_model, x, y, cv=cv)
    scores['SGD'] = score_value.mean()
    return scores


def plot_from_dict(d, kind=None, title=None, legend=False, ylim=None):
    PlotData.from_dict(d, orient='index').plot(kind=kind, title=title, legend=legend, ylim=ylim)
